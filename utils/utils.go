package utils

import (
	"adsMaker/db"
	"database/sql"
	"net/http"
)

func Session(_db *sql.DB, w http.ResponseWriter, req *http.Request) (db.Session, error) {
	cookie, err := req.Cookie("_cookie")
	if err != nil {
		return db.Session{}, err
	}

	sess := db.Session{}
	return sess.Check(_db, cookie.Value)
}

func WrapCall(
	_db *sql.DB,
	a func(_db *sql.DB, w http.ResponseWriter, req *http.Request),
) func(w http.ResponseWriter, req *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		a(_db, w, req)
	}
}
