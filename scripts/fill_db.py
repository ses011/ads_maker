import psycopg2
import random
import os


adj = ['enormous', 'pro', 'good', 'fucked up', 'cursed', 'magnificent', 'beautiful', 'bad', 'worst', 'angry', 'big',
		'small', 'old', 'new', 'rectangular', 'red', 'orange', 'pretty', 'steel', 'Halloween', 'large', 'good looking']

titles = ['flopa', 'Samsung', 'notebook', 'MacBook', 'Pro', 'IMac', 'задержка',
		  'BOSS ds-1', 'shoes', 'тетрадь 12 листов купить оптом', 'video games for pc', 'шоха деда',
		  'Дача недалеко от города', 'Lego 1', 'Lego 2', 'Nurf blaster 1', 'Nurf blaster 2', 'case for iphone',
		  'Pink case for phone', 'scientific microscope', 'microscope toy', 'Professional skateboard',
		  'Skateboard for begginers', 'Fender stratocaster', 'Gibson SG', 'Backpack for men', 'backpack', 'monitor',
		  'phone', 'meme', 'toy', 'blaster', 'kit', 'dota 2', 'anime', 'anime poster', 'cartoon', 'random']
photos = ['https://static.wikia.nocookie.net/floppa/images/9/96/Floppa.jpg/revision/latest?cb=20200815053935',
		  'https://www.ixbt.com/img/n1/news/2019/0/2/Notebook-9-Pro_main_1_large.jpg',
		  'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/MacBook_Pro_15_inch_%282017%29_Touch_Bar.jpg/1200px-MacBook_Pro_15_inch_%282017%29_Touch_Bar.jpg',
		  'https://64.img.avito.st/640x480/10084991764.jpg',
		  'https://dmtrpedals.ru/wp-content/uploads/2018/03/IMG_4535.jpg',
		  'https://upload.wikimedia.org/wikipedia/commons/a/ac/DS_1_Distortion.jpg',
		  'https://img.thrfun.com/img/002/234/dirty_shoes_x.jpg',
		  'https://static.detmir.st/media_out/961/915/1915961/1500/0.jpg',
		  'https://cache3.youla.io/files/images/720_720_out/5c/75/5c7571e6c15ae3510a5e1555.jpg',
		  'https://cdnimg.rg.ru/img/content/186/17/88/1212122_d_850.jpg',
		  'https://icdn.lenta.ru/images/2019/05/27/10/20190527105727424/square_320_c95ff490a6b83009b1978d0a9754242c.jpg',
		  'https://www.ikea.com/ru/ru/images/products/bygglek-byugglek-konstruktor-lego-r-201-detal-raznye-cveta__0915518_pe784785_s5.jpg',
		  'https://www.lego.com/cdn/cs/set/assets/blt8c4442c8dd1ff2bf/76393.jpg',
		  'https://www.ikea.com/ru/ru/images/products/bygglek-byugglek-konstruktor-lego-r-201-detal-raznye-cveta__0915518_pe784785_s5.jpg',
		  'https://95.img.avito.st/640x480/6105962395.jpg',
		  'https://i.pinimg.com/originals/b6/65/45/b665456dbae1f63c47ca2c7cdd915719.jpg',
		  'https://i.ebayimg.com/images/g/VJQAAOSwRjVdcnzj/s-l300.jpg',
		  'https://ae01.alicdn.com/kf/Hb3e65f894deb4a2498d80caee339c3c8S/Toyota-RAV4-RAV-4-2013-2017.jpg_q50.jpg',
		  'https://images-na.ssl-images-amazon.com/images/I/81-1xs0GrpL._SL1500_.jpg',
		  'https://ichef.bbci.co.uk/news/976/cpsprodpb/BDB1/production/_113616584_gun1.jpg',
		  'https://img2.wbstatic.net/big/new/8570000/8575662-1.jpg',
		  'https://img2.wbstatic.net/big/new/12620000/12623806-1.jpg',
		  'https://mallbg.com/products/or156125_1.jpg',
		  'https://target.scene7.com/is/image/Target/GUEST_b8d1415c-ad13-4ea0-b304-6e22afd0ee88?wid=488&hei=488&fmt=pjpeg',
		  'https://cdn-ssl.s7.disneystore.com/is/image/DisneyShopping/7409059766276',
		  'https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/MUJP2?wid=1144&hei=1144&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1543449124294',
		  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWaYU2To22QxZhSonJWmq5-5l6-h_5EhnS4w&usqp=CAU',
		  'https://static.bhphoto.com/images/images1000x1000/1341223635_870291.jpg',
		  'https://cdn.shopify.com/s/files/1/0274/4293/7933/products/001074659_804x.progressive.jpg?v=1597113203',
		  'https://contents.mediadecathlon.com/p1726514/k$7147a4189f78774d93d20eb4566e3d32/skateboard-complete-100-parrot.jpg?&f=x',
		  'https://images.monoprice.com/productlargeimages/6101641.jpg',
		  'https://media.guitarcenter.com/is/image/MMGS7//SG-Standard-Electric-Guitar-Ebony/L54573000002000-00-1600x1600.jpg',
		  'https://cdn1.static-tgdp.com/ui/productimages/approved/std.lang.all/40/96/634096_sized_1800x1200_rev_1.jpg',
		  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKTQ9jH9CVO7pbjpGFERk85wGHrgLFmR7Nhg&usqp=CAU',
		  'https://cs13.pikabu.ru/post_img/big/2021/03/10/2/1615339500157142015.jpg',
		  'https://images-na.ssl-images-amazon.com/images/I/81s%2BjxE5KEL._AC_SY741_.jpg',
		  'https://images-na.ssl-images-amazon.com/images/I/61Zm2hsRJ0L._AC_SY679_.jpg',
		  'https://ae04.alicdn.com/kf/Hf62ae40bb8cf43149531f579a0fa5376v/-.jpg',
		  'https://img.fruugo.com/product/7/92/32862927_max.jpg',
		  'https://hoff.ru/upload/iblock/b43/b4313e48e8f6fe7912a0ee08904d7819.jpg',
		  'https://matras-strong.ru/upload/iblock/830/8300c544a20447ff9d5ebebc7a4f11e4.jpg',
		  'http://mebelnyj.ru/uploads/posts/2017-09/1505486999_3.jpg',
		  'https://img.mvideo.ru/Pdb/50126638b.jpg',
		  'https://3dnews.ru/assets/external/illustrations/2021/06/23/1042714/432778478.jpg',
		  'https://www.ixbt.com/img/n1/news/2021/5/1/samsung-galaxy-m51-pers-1_large.jpg',
		  'https://images.samsung.com/is/image/samsung/assets/ru/galaxy-a52/buy/a-72/A72_AwesomeViolet_ProductKV_MO_img.jpg',
		  'https://images.samsung.com/ru/smartphones/galaxy-z-flip/buy/carousel/3-Bloom-Purple-Gallery-Mobile-img.jpg',
		  'https://mobiltelefon.ru/photo/june21/02/znakomtes_sem_samsung_izbavlaetsa_ot_assistenta_bixby_picture2_0_resize.jpg',
		  'https://kakuchopurei.com/wp-content/uploads/2021/06/Samsung-Girl.jpg',
		  'https://main-cdn.goods.ru/big1/hlr-system/1573360124/100025945335b0.jpg',
		  'https://cache3.youla.io/files/images/720_720_out/5c/0a/5c0abcda22a449c4d172b278.jpg',
		  ]


def main():
	dbname = os.environ['NAME']
	user = os.environ['USER']
	password = os.environ['PSWD']
	host = os.environ['HOST']

	conn = psycopg2.connect(dbname=dbname, user=user,
    	                    password=password, host=host)
	cursor = conn.cursor()

	db_req_ads = "INSERT into ads (user_login, title, description, price) values "
	db_req_photos = "INSERT into photo_urls (photo_url, aid) values"
	s_ads = ''
	s_photos = ''

	adj_n = len(adj)
	titles_n = len(titles)
	z = 0

	db_req_user = "INSERT into users (login, password, initials) values (\'test\', \'test\', \'test\');"
	cursor.execute(db_req_user)
	conn.commit()

	while len(photos) > 0:
		z += 1
		photos_n = len(photos)
		if photos_n <= 3:
			for i in range(photos_n):
				s_photos += f'(\'{photos.pop()}\', x~x~x), '
		else:
			n = random.randint(1, 3)
			for i in range(n):
				tmp = random.randint(0, len(photos)-1)
				s_photos += f'(\'{photos.pop(tmp)}\', x~x~x), '

		a = random.randint(0, adj_n-1)
		t = random.randint(0, titles_n-1)
		s_ads += f'(\'test\', \'{adj[a]} {titles[t]}\', \'test add {z}\', {random.randint(500, 70000)}), '

	cursor.execute(db_req_ads + s_ads[:len(s_ads)-2:] + 'returning aid'+ ';')
	aids = cursor.fetchall()
	conn.commit()

	i = 0
	m = len(aids)
	while 'x~x~x' in s_photos:
		s_photos = s_photos.replace('x~x~x', str(aids[i][0]), 1)
		i += 1
		if i == m:
			i = 0

	cursor.execute(db_req_photos + s_photos[:len(s_photos)-2:] + ';')
	conn.commit()

	conn.close()


main()