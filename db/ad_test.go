package db

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
	"time"
)

func TestAdGetUser(t *testing.T) {
	columns := []string{"uid", "login", "password", "initials"}

	t.Run("user exist", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			UserLogin: "foo",
		}

		u, err := a.GetUser(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, "foo", u.Login)
		assert.Equal(t, "123", u.Password)
		assert.Equal(t, "asdf", u.Initials)
	})

	t.Run("user not exist", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`)

		a := Ad{
			UserLogin: "foo",
		}

		u, err := a.GetUser(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
		assert.Equal(t, User{}, u)
	})
}

func TestAdGetUserInitials(t *testing.T) {
	columns := []string{"uid", "login", "password", "initials"}

	t.Run("user exist", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			UserLogin: "foo",
		}

		initials := a.GetUserInitials(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, "asdf", initials)
	})

	t.Run("user not exist", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`)

		a := Ad{
			UserLogin: "foo",
		}

		initials := a.GetUserInitials(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, "UNKNOWN", initials)
	})
}

func TestAdGetStatusCodeAd(t *testing.T) {
	columns := []string{"uid", "login", "password", "initials"}

	t.Run("valid 1 photo", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "test",
			PhotoUrls:   []string{"p1"},
			Price:       123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, 0, res)
	})

	t.Run("valid 2 photo", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "test",
			PhotoUrls:   []string{"p1", "p2"},
			Price:       123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, 0, res)
	})

	t.Run("valid 3 photo", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "test",
			PhotoUrls:   []string{"p1", "p2", "p3"},
			Price:       123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, 0, res)
	})

	t.Run("not valid 4 photo", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "test",
			PhotoUrls:   []string{"p1", "p2", "p3", "p4"},
			Price:       123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, IncorrectPhotosAmount, res)
	})

	t.Run("not valid, no user", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		a := Ad{
			Aid:         1,
			UserLogin:   "",
			Title:       "test",
			PhotoUrls:   []string{"p1"},
			Price:       123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, NoUserLogin, res)
	})

	t.Run("not valid, negative price", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "test",
			PhotoUrls:   []string{"p1"},
			Price:       -123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, NegativePrice, res)
	})

	t.Run("not valid, no title", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "",
			PhotoUrls:   []string{"p1"},
			Price:       123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, NoTitle, res)
	})

	t.Run("not valid, incorrect user", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`)

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "test",
			PhotoUrls:   []string{"p1"},
			Price:       123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, IncorrectUserLogin, res)
	})

	t.Run("not valid, large title", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       strings.Repeat("3", 212),
			PhotoUrls:   []string{"p1"},
			Price:       123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, IncorrectTitle, res)
	})

	t.Run("not valid, large description", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "test",
			PhotoUrls:   []string{"p1"},
			Price:       123,
			Description: strings.Repeat("abcde", 201),
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, IncorrectDescription, res)
	})

	t.Run("not valid, no photos 1", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "test",
			PhotoUrls:   []string{},
			Price:       123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, NoPhotos, res)
	})

	t.Run("not valid, no photos 1", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "test",
			PhotoUrls:   []string{""},
			Price:       123,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, NoPhotos, res)
	})

	t.Run("not valid, no price", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		a := Ad{
			Aid:         1,
			UserLogin:   "foo",
			Title:       "test",
			PhotoUrls:   []string{"p1"},
			Price:       0,
			Description: "test descr",
			CreatedAt:   time.Now(),
		}

		res := a.GetStatusCode(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, NoPrice, res)
	})
}

func TestAdGetByAid(t *testing.T) {
	columns := []string{"aid", "user_login", "title", "description", "price", "created_at"}

	t.Run("ad exists, 1 photo", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "title", "description", 123, time.Now())).
			RowsWillBeClosed()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1")).RowsWillBeClosed()

		a, err := GetByAid(db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, 1, a.Aid)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, "foo", a.UserLogin)
		assert.Equal(t, 123, a.Price)
		assert.Equal(t, "description", a.Description)
		assert.Equal(t, 1, len(a.PhotoUrls))
		assert.Equal(t, "p1", a.PhotoUrls[0])
	})

	t.Run("ad exists, 2 photo", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "title", "description", 123, time.Now())).
			RowsWillBeClosed()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1").AddRow("p2")).
			RowsWillBeClosed()

		a, err := GetByAid(db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, 1, a.Aid)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, "foo", a.UserLogin)
		assert.Equal(t, 123, a.Price)
		assert.Equal(t, "description", a.Description)
		assert.Equal(t, 2, len(a.PhotoUrls))
		assert.Equal(t, "p1", a.PhotoUrls[0])
		assert.Equal(t, "p2", a.PhotoUrls[1])
	})

	t.Run("ad exists, 3 photo", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "title", "description", 123, time.Now())).
			RowsWillBeClosed()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1").AddRow("p2").AddRow("p3")).
			RowsWillBeClosed()

		a, err := GetByAid(db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, 1, a.Aid)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, "foo", a.UserLogin)
		assert.Equal(t, 123, a.Price)
		assert.Equal(t, "description", a.Description)
		assert.Equal(t, 3, len(a.PhotoUrls))
		assert.Equal(t, "p1", a.PhotoUrls[0])
		assert.Equal(t, "p2", a.PhotoUrls[1])
		assert.Equal(t, "p3", a.PhotoUrls[2])
	})

	t.Run("ad not exists", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`)

		a, err := GetByAid(db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
		assert.Equal(t, Ad{}, a)
	})

	t.Run("ad exists, photos not exists", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "title", "description", 123, time.Now())).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`)

		a, err := GetByAid(db, 1)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
		assert.Equal(t, Ad{}, a)
	})
}

func TestAdGetByAidWithParams(t *testing.T) {
	columns := []string{"title", "price"}

	t.Run("ad exists, without anything", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow("title", 123)).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1")).RowsWillBeClosed()

		a, err := GetByAidWithParams(db, 1, false, false)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, "", a.Description)
		assert.Equal(t, 123, a.Price)
		assert.Equal(t, 1, len(a.PhotoUrls))
		assert.Equal(t, "p1", a.PhotoUrls[0])
	})

	t.Run("ad exists, only description", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(append(columns, "description")).
				AddRow("title", 123, "description")).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1")).RowsWillBeClosed()

		a, err := GetByAidWithParams(db, 1, true, false)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, "description", a.Description)
		assert.Equal(t, 123, a.Price)
		assert.Equal(t, 1, len(a.PhotoUrls))
		assert.Equal(t, "p1", a.PhotoUrls[0])
	})

	t.Run("ad exists, only all_photos (1)", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow("title", 123)).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1")).RowsWillBeClosed()

		a, err := GetByAidWithParams(db, 1, false, true)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, "", a.Description)
		assert.Equal(t, 123, a.Price)
		assert.Equal(t, 1, len(a.PhotoUrls))
		assert.Equal(t, "p1", a.PhotoUrls[0])
	})

	t.Run("ad exists, only all_photos (2)", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow("title", 123)).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1").AddRow("p2")).
			RowsWillBeClosed()

		a, err := GetByAidWithParams(db, 1, false, true)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, "", a.Description)
		assert.Equal(t, 123, a.Price)
		assert.Equal(t, 2, len(a.PhotoUrls))
		assert.Equal(t, "p1", a.PhotoUrls[0])
		assert.Equal(t, "p2", a.PhotoUrls[1])
	})

	t.Run("ad exists, only all_photos (3)", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow("title", 123)).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).
				AddRow("p1").AddRow("p2").AddRow("p3")).
			RowsWillBeClosed()

		a, err := GetByAidWithParams(db, 1, false, true)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, "", a.Description)
		assert.Equal(t, 123, a.Price)
		assert.Equal(t, 3, len(a.PhotoUrls))
		assert.Equal(t, "p1", a.PhotoUrls[0])
		assert.Equal(t, "p2", a.PhotoUrls[1])
		assert.Equal(t, "p3", a.PhotoUrls[2])
	})

	t.Run("ad exists, description + all_photos", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(append(columns, "description")).
				AddRow("title", 123, "description")).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1").
				AddRow("p2").AddRow("p3")).
			RowsWillBeClosed()

		a, err := GetByAidWithParams(db, 1, true, true)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, "description", a.Description)
		assert.Equal(t, 123, a.Price)
		assert.Equal(t, 3, len(a.PhotoUrls))
		assert.Equal(t, "p1", a.PhotoUrls[0])
		assert.Equal(t, "p2", a.PhotoUrls[1])
		assert.Equal(t, "p3", a.PhotoUrls[2])
	})

	t.Run("ad not exists", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`)

		a, err := GetByAidWithParams(db, 1, false, true)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
		assert.Equal(t, Ad{}, a)
	})

	t.Run("ad not exists", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(append(columns, "description")).
				AddRow("title", 123, "description")).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`)

		a, err := GetByAidWithParams(db, 1, true, true)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
		assert.Equal(t, Ad{}, a)
	})
}

func TestAdGetNumOfAds(t *testing.T) {
	t.Run("correct", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(10)).
			RowsWillBeClosed()

		c := GetNumOfAds(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, 10, c)
	})

	t.Run("correct 0", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0)).
			RowsWillBeClosed()

		c := GetNumOfAds(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, 0, c)
	})

	t.Run("incorrect", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`)

		c := GetNumOfAds(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, -1, c)
	})
}

func TestAdAddToDb(t *testing.T) {
	db, mock, _ := sqlmock.New()
	defer db.Close()

	q := `INSERT INTO ads (.+) returning aid`

	mock.ExpectQuery(q).
		WithArgs("foo", "title", "description", 123).
		WillReturnRows(sqlmock.NewRows([]string{"aid"}).AddRow(1)).
		RowsWillBeClosed()
	mock.ExpectExec("INSERT INTO photo_urls").
		WithArgs("p1", 1).
		WillReturnResult(sqlmock.NewResult(1, 1))

	a := Ad{
		UserLogin:   "foo",
		Title:       "title",
		Description: "description",
		Price:       123,
		PhotoUrls:   []string{"p1"},
	}

	aid, err := a.AddToDb(db)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.NoError(t, err)
	assert.Equal(t, 1, aid)
}
