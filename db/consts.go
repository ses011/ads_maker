package db

const (
	OK int = 0
)

// 1x
const (
	NoUserLogin int = iota + 11
	IncorrectUserLogin
	NoTitle
	IncorrectTitle
	NoPhotos
	IncorrectPhotosAmount
	NoPrice
	NegativePrice
	IncorrectDescription
)

// 4x
const (
	NoLogin int = iota + 41
	IncorrectLogin
	NoPassword
	IncorrectPassword
	NoInitials
	IncorrectInitials
)

const (
	MaxTitleLen       int = 200
	MaxPhotos         int = 3
	MaxDescriptionLen int = 1000
)

const (
	MaxLoginLen int = 20
	MinPassLen  int = 8
	MaxPassLen  int = 40
	MaxInitLen  int = 60
)

const (
	MaxPhotoPage int = 10
)
