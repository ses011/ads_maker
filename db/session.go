package db

import (
	"database/sql"
	"errors"
	sq "github.com/Masterminds/squirrel"
	"time"
)

type Session struct {
	Id        int
	UserId    int
	Login     string
	Uuid      string
	CreatedAt time.Time
}

func (s *Session) Check(db *sql.DB, uuid string) (Session, error) {
	r := sq.Select("id", "uuid", "login", "user_id", "created_at").From("sessions").Where(sq.Eq{"uuid": uuid}).PlaceholderFormat(sq.Dollar)
	if err := r.RunWith(db).QueryRow().Scan(&s.Id, &s.Uuid, &s.Login, &s.UserId, &s.CreatedAt); err != nil {
		return Session{}, err
	}
	if s.Id == 0 {
		return Session{}, errors.New("Invalid Session")
	}
	return *s, nil
}

func (s *Session) DeleteByUuid(db *sql.DB) error {
	r := sq.Delete("sessions").Where(sq.Eq{"uuid": s.Uuid}).PlaceholderFormat(sq.Dollar)
	_, err := r.RunWith(db).Exec()
	return err
}

func (s *Session) GetUser(db *sql.DB) (User, error) {
	u, err := GetByLogin(db, s.Login)
	if err != nil {
		return User{}, err
	}
	return u, nil
}
