package db

import (
	"crypto/rand"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"os"
)

var (
	Db *sql.DB
)

var (
	host     = os.Getenv("HOST")
	port     = os.Getenv("PORT")
	user     = os.Getenv("USER")
	password = os.Getenv("PSWD")
	dbname   = os.Getenv("NAME")
)

var (
	createTableTemplates = []string{
		`CREATE TABLE IF NOT EXISTS users (
			uid serial PRIMARY KEY,
			login varchar(20) NOT NULL UNIQUE,
			password varchar(40) NOT NULL,
			initials varchar(60) NOT NULL
		)`,

		`CREATE TABLE IF NOT EXISTS sessions (
			id serial PRIMARY KEY,
			uuid varchar(64) UNIQUE,
			login varchar(20) NOT NULL,
			user_id integer REFERENCES users(uid) ON DELETE CASCADE,
			created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
	    )`,

		`CREATE TABLE IF NOT EXISTS ads (
			aid serial PRIMARY KEY,
			title varchar(200) NOT NULL,
			description varchar(1000),
			price numeric CHECK (price > 0),
			created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
			user_login varchar(20) REFERENCES users(login) ON DELETE CASCADE
		)`,

		`CREATE TABLE  IF NOT EXISTS photo_urls (
		id serial PRIMARY KEY,
		photo_url varchar(1000) NOT NULL,
		aid integer REFERENCES ads(aid) ON DELETE CASCADE
		)`,
	}
)

func init() {
	if len(os.Args) > 1 && os.Args[1][:5] == "-test" {
		return
	}

	psqlInfo := fmt.Sprintf("host=%v port=%v user=%v password=%v dbname=%v sslmode=disable",
		host, port, user, password, dbname)
	db1, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	Db = db1

	for _, template := range createTableTemplates {
		if _, err = Db.Exec(template); err != nil {
			log.Fatal(err)
		}
	}
}

func createUUID() string {
	u := new([16]byte)
	_, err := rand.Read(u[:])
	if err != nil {
		log.Fatalln("Cannot generate UUID", err)
	}
	u[8] = (u[8] | 0x40) & 0x7F
	u[6] = (u[6] & 0xF) | (0x4 << 4)
	return fmt.Sprintf("%x-%x-%x-%x-%x", u[0:4], u[4:6], u[6:8], u[8:10], u[10:])
}
