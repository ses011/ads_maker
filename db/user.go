package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"time"
)

type User struct {
	Uid      int    `json:"-"`
	Login    string `json:"login"`
	Password string `json:"-"`
	Initials string `json:"initials"`
}

func (u User) String() string {
	res := fmt.Sprintf("%v %v %v %v", u.Uid, u.Login, u.Password, u.Initials)
	return res
}

func GetByLogin(_db *sql.DB, login string) (User, error) {
	var u User
	s := sq.Select("uid", "login", "password", "initials").From("users").Where(sq.Eq{"login": login}).PlaceholderFormat(sq.Dollar)
	if err := s.RunWith(_db).QueryRow().Scan(&u.Uid, &u.Login, &u.Password, &u.Initials); err != nil {
		return User{}, err
	}
	return u, nil
}

func (u *User) AlreadyExists(_db *sql.DB) bool {
	res := 0
	s := sq.Select("count(uid)").From("users").Where(sq.Eq{"login": u.Login}).PlaceholderFormat(sq.Dollar)
	if err := s.RunWith(_db).QueryRow().Scan(&res); err != nil {
		res = 0
	}
	if res == 0 {
		return false
	}
	return true
}

func (u User) Json() ([]byte, error) {
	return json.Marshal(u)
}

func (u User) GetStatusCode() int {
	if u.Login == "" {
		return NoLogin
	}
	if len(u.Login) > MaxLoginLen {
		return IncorrectLogin
	}
	if u.Password == "" {
		return NoPassword
	}
	if len(u.Password) < MinPassLen || len(u.Password) > MaxPassLen {
		return IncorrectPassword
	}
	if u.Initials == "" {
		return NoInitials
	}
	if len(u.Initials) > MaxInitLen {
		return IncorrectInitials
	}
	return OK
}

func (u User) AddToDb(_db *sql.DB) error {
	r := sq.Insert("users").Columns("login", "password", "initials").
		Values(u.Login, u.Password, u.Initials)
	if _, err := r.RunWith(_db).PlaceholderFormat(sq.Dollar).Exec(); err != nil {
		return err
	} else {
		return nil
	}
}

func (u *User) CreateSession(_db *sql.DB) (Session, error) {
	uuid := createUUID()
	i1 := sq.Insert("sessions").Columns("uuid", "login", "user_id", "created_at").
		Values(uuid, u.Login, u.Uid, time.Now()).PlaceholderFormat(sq.Dollar)
	if _, err := i1.RunWith(_db).Exec(); err != nil {
		return Session{}, err
	}

	s := Session{}
	s1 := sq.Select("id", "uuid", "login", "user_id", "created_at").From("sessions").Where(sq.Eq{"uuid": uuid}).PlaceholderFormat(sq.Dollar)
	if err := s1.RunWith(_db).QueryRow().Scan(&s.Id, &s.Uuid, &s.Login, &s.UserId, &s.CreatedAt); err != nil {
		return Session{}, err
	}
	return s, nil
}
