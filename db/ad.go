package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"time"
)

type Ad struct {
	Aid         int       `json:"-"`
	UserLogin   string    `json:"user_id,omitempty"`
	Title       string    `json:"title"`
	PhotoUrls   []string  `json:"photo_url"`
	Price       int       `json:"price"`
	Description string    `json:"description,omitempty"`
	CreatedAt   time.Time `json:"-"`
}

func (a Ad) String() string {
	res := fmt.Sprintf("Id: %d\nLogin: %s\nTitle: %s\nDescription: %s\nPhoto url: %s\nPrice: %dp",
		a.Aid, a.UserLogin, a.Title, a.Description, a.PhotoUrls[0], a.Price)
	return res
}

func (a Ad) GetUser(_db *sql.DB) (User, error) {
	res, err := GetByLogin(_db, a.UserLogin)
	if err != nil {
		return User{}, err
	}
	return res, nil
}

func (a Ad) GetUserInitials(_db *sql.DB) string {
	u, err := a.GetUser(_db)
	if err != nil {
		return "UNKNOWN"
	}
	return u.Initials
}

func (a Ad) CreatedAtDate() string {
	return a.CreatedAt.Format("Jan 2, 2006 at 3:04pm")
}

func (a Ad) AddToDb(_db *sql.DB) (int, error) {
	q := sq.Insert("ads").Columns("user_login", "title", "description", "price").
		Values(a.UserLogin, a.Title, a.Description, a.Price).Suffix("returning aid").
		RunWith(_db).PlaceholderFormat(sq.Dollar)
	if err := q.QueryRow().Scan(&a.Aid); err != nil {
		return 0, err
	}
	iQ := sq.Insert("photo_urls").Columns("photo_url", "aid")
	for _, url := range a.PhotoUrls {
		iQ = iQ.Values(url, a.Aid).PlaceholderFormat(sq.Dollar)
	}
	_, err := iQ.RunWith(_db).Exec()
	if err != nil {
		return 0, err
	}
	return a.Aid, nil
}

func (a Ad) Json() ([]byte, error) {
	return json.Marshal(a)
}

func (a Ad) GetStatusCode(_db *sql.DB) int {
	if a.UserLogin == "" {
		return NoUserLogin
	}
	u, err := a.GetUser(_db)
	if err != nil || u.Login == "" {
		return IncorrectUserLogin
	}
	if a.Title == "" {
		return NoTitle
	}
	if len(a.Title) > MaxTitleLen {
		return IncorrectTitle
	}
	if len(a.Description) > MaxDescriptionLen {
		return IncorrectDescription
	}
	if len(a.PhotoUrls) == 0 || (len(a.PhotoUrls) == 1 && a.PhotoUrls[0] == "") {
		return NoPhotos
	}
	if len(a.PhotoUrls) > MaxPhotos {
		return IncorrectPhotosAmount
	}
	if a.Price == 0 {
		return NoPrice
	}
	if a.Price < 0 {
		return NegativePrice
	}
	return OK
}

func GetByAidWithParams(_db *sql.DB, aid int, description bool, allPhotos bool) (Ad, error) {
	res := Ad{}
	cols := "title, price"
	if description {
		cols += ", description"
		r1 := sq.Select(cols).From("ads").Where(sq.Eq{"aid": aid}).PlaceholderFormat(sq.Dollar)
		if err := r1.RunWith(_db).QueryRow().Scan(&res.Title, &res.Price, &res.Description); err != nil {
			return Ad{}, err
		}
	} else {
		r1 := sq.Select(cols).From("ads").Where(sq.Eq{"aid": aid}).PlaceholderFormat(sq.Dollar)
		if err := r1.RunWith(_db).QueryRow().Scan(&res.Title, &res.Price); err != nil {
			return Ad{}, err
		}
	}
	limit := 1
	if allPhotos {
		limit = 3
	}
	r2 := sq.Select("photo_url").From("photo_urls").Where(sq.Eq{"aid": aid}).
		PlaceholderFormat(sq.Dollar).Limit(uint64(limit))
	rows, err := r2.RunWith(_db).PlaceholderFormat(sq.Dollar).Query()
	if err != nil {
		return Ad{}, err
	}
	url := ""
	for rows.Next() {
		if err := rows.Scan(&url); err != nil {
			return res, err
		}
		res.PhotoUrls = append(res.PhotoUrls, url)
	}
	return res, nil
}

func GetByAid(_db *sql.DB, aid int) (Ad, error) {
	res := Ad{}
	r1 := sq.Select("aid, user_login, title, description, price, created_at").From("ads").
		Where(sq.Eq{"Aid": aid})
	if err := r1.RunWith(_db).PlaceholderFormat(sq.Dollar).QueryRow().
		Scan(&res.Aid, &res.UserLogin, &res.Title, &res.Description, &res.Price, &res.CreatedAt); err != nil {
		return Ad{}, err
	}
	r2 := sq.Select("photo_url").From("photo_urls").Where(sq.Eq{"aid": res.Aid})
	rows, err := r2.RunWith(_db).PlaceholderFormat(sq.Dollar).Query()
	if err != nil {
		return Ad{}, err
	}
	url := ""
	for rows.Next() {
		if err := rows.Scan(&url); err != nil {
			return res, err
		}
		res.PhotoUrls = append(res.PhotoUrls, url)
	}
	return res, nil
}

func GetList(_db *sql.DB, sort string, order string, offset uint64) ([]Ad, error) {
	var res []Ad
	sqReq := "select ads.aid, ads.title, photo_url, price " +
		"from ads join (select distinct on (aid) aid, photo_url from photo_urls) AS p on p.aid = ads.aid" +
		" order by %s %s limit 10 offset %d"
	if sort != "" {
		sqReq = fmt.Sprintf(sqReq, sort, order, offset)
	} else {
		sqReq = fmt.Sprintf(sqReq, "created_at", order, offset)
	}
	rows, err := _db.Query(sqReq)
	if err != nil {
		return res, err
	}
	for rows.Next() {
		var a Ad
		a.PhotoUrls = append(a.PhotoUrls, "")
		if err := rows.Scan(&a.Aid, &a.Title, &a.PhotoUrls[0], &a.Price); err != nil {
			return res, err
		}
		res = append(res, a)
	}
	return res, nil
}

func GetNumOfAds(_db *sql.DB) int {
	var res int
	s := sq.Select("count(aid)").From("ads")
	if err := s.RunWith(_db).QueryRow().Scan(&res); err != nil {
		return -1
	}
	return res
}
