package db

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestSessionCheck(t *testing.T) {
	columns := []string{"id", "uuid", "login", "user_id", "created_at"}

	t.Run("correct", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		_time := time.Now()

		mock.ExpectQuery(`SELECT`).WillReturnRows(sqlmock.NewRows(columns).
			AddRow(1, "test_uuid", "foo", 1, _time)).
			RowsWillBeClosed()

		s := Session{}
		s, err := s.Check(db, "test_uuid")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, _time, s.CreatedAt)
		assert.Equal(t, 1, s.UserId)
		assert.Equal(t, "foo", s.Login)
		assert.Equal(t, "test_uuid", s.Uuid)
		assert.Equal(t, 1, s.Id)
	})

	t.Run("incorrect", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`)

		s := Session{}
		s, err := s.Check(db, "test_uuid")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
		assert.Equal(t, Session{}, s)
	})

	t.Run("correct, id = 0", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		_time := time.Now()

		mock.ExpectQuery(`SELECT`).WillReturnRows(sqlmock.NewRows(columns).
			AddRow(0, "test_uuid", "foo", 1, _time)).
			RowsWillBeClosed()

		s := Session{}
		s, err := s.Check(db, "test_uuid")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
		assert.Equal(t, Session{}, s)
	})
}

func TestSessionDeleteByUuid(t *testing.T) {
	t.Run("correct", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectExec("DELETE FROM sessions").WillReturnResult(sqlmock.NewResult(1, 1))

		s := Session{
			Id:        1,
			Login:     "foo",
			UserId:    1,
			Uuid:      "test_uuid",
			CreatedAt: time.Now(),
		}

		err := s.DeleteByUuid(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
	})

	t.Run("incorrect", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectExec("DELETE FROM sessions")

		s := Session{
			Id:        1,
			Login:     "foo",
			UserId:    1,
			Uuid:      "test_uuid",
			CreatedAt: time.Now(),
		}

		err := s.DeleteByUuid(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
	})
}

func TestSessionGetUser(t *testing.T) {
	columns := []string{"uid", "login", "password", "initials"}

	t.Run("correct", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		s := &Session{
			Id:        1,
			Login:     "foo",
			UserId:    1,
			Uuid:      "test_uuid",
			CreatedAt: time.Now(),
		}

		u, err := s.GetUser(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, "foo", u.Login)
	})

	t.Run("incorrect", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`)

		s := &Session{
			Id:        1,
			Login:     "foo",
			UserId:    1,
			Uuid:      "test_uuid",
			CreatedAt: time.Now(),
		}

		u, err := s.GetUser(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
		assert.Equal(t, User{}, u)
	})
}
