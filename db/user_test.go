package db

import (
	"database/sql/driver"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
	"time"
)

func TestUserGetStatusCodeUser(t *testing.T) {
	largePswd := strings.Repeat("1", 78)
	largeLogin := strings.Repeat("2", 78)
	largeInitials := strings.Repeat("3", 78)
	var tests = []struct {
		input User
		want  int
	}{
		{User{Uid: 0, Login: "test", Password: "12345678", Initials: "initials"}, OK},
		{User{Uid: 0, Login: "", Password: "12345678", Initials: "initials"}, NoLogin},
		{User{Uid: 0, Login: largeLogin, Password: "12345678", Initials: "initials"}, IncorrectLogin},
		{User{Uid: 0, Login: "test", Password: "", Initials: "initials"}, NoPassword},
		{User{Uid: 0, Login: "test", Password: "123456", Initials: "initials"}, IncorrectPassword},
		{User{Uid: 0, Login: "test", Password: largePswd, Initials: "initials"}, IncorrectPassword},
		{User{Uid: 0, Login: "test", Password: "12345678", Initials: ""}, NoInitials},
		{User{Uid: 0, Login: "test", Password: "12345678", Initials: largeInitials}, IncorrectInitials},
	}

	for _, test := range tests {
		assert.Equal(t, test.want, test.input.GetStatusCode())
	}
}

func TestUserGetByLogin(t *testing.T) {
	columns := []string{"uid", "login", "password", "initials"}

	t.Run("user exist", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow(1, "foo", "123", "asdf")).
			RowsWillBeClosed()

		user, err := GetByLogin(db, "foo")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.NoError(t, err)
		assert.Equal(t, User{
			Uid:      1,
			Login:    "foo",
			Password: "123",
			Initials: "asdf",
		}, user)
	})

	t.Run("not exist", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`)

		user, err := GetByLogin(db, "bar")

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Error(t, err)
		assert.Equal(t, User{}, user)
	})
}

func TestUserAlreadyExists(t *testing.T) {
	t.Run("user exist", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(1)).
			RowsWillBeClosed()

		u := User{
			Uid:      1,
			Login:    "foo",
			Password: "123",
			Initials: "asdf",
		}

		res := u.AlreadyExists(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.True(t, res)
	})

	t.Run("user not exist", func(t *testing.T) {

		db, mock, _ := sqlmock.New()
		defer db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0)).
			RowsWillBeClosed()

		u := User{
			Uid:      1,
			Login:    "foo",
			Password: "123",
			Initials: "asdf",
		}
		res := u.AlreadyExists(db)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.False(t, res)
	})
}

func TestUserAddToDb(t *testing.T) {

	db, mock, _ := sqlmock.New()
	defer db.Close()

	mock.ExpectExec("INSERT INTO users").
		WithArgs("foo", "123", "asdf").
		WillReturnResult(sqlmock.NewResult(1, 1))

	user := User{
		Login:    "foo",
		Password: "123",
		Initials: "asdf",
	}

	err := user.AddToDb(db)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.NoError(t, err)
}

type AnyTime struct{}

func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

type AnyString struct{}

func (a AnyString) Match(v driver.Value) bool {
	_, ok := v.(string)
	return ok
}

func TestCreateSession(t *testing.T) {
	t.Parallel()
	db, mock, _ := sqlmock.New()
	defer db.Close()

	u := User{
		Uid:      1,
		Login:    "foo",
		Password: "123",
		Initials: "asdf",
	}

	mock.ExpectExec("INSERT INTO sessions").
		WithArgs(AnyString{}, "foo", 1, AnyTime{}).
		WillReturnResult(sqlmock.NewResult(1, 1))

	mock.ExpectQuery(`SELECT`).
		WillReturnRows(sqlmock.NewRows([]string{"id", "uuid", "login", "user_id", "created_at"}).AddRow(1, "kasldksa", "foo", 1, time.Now())).
		RowsWillBeClosed()

	s, err := u.CreateSession(db)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.NoError(t, err)
	assert.Equal(t, u.Login, s.Login)
	assert.Equal(t, u.Uid, s.UserId)
	assert.Equal(t, "kasldksa", s.Uuid)
}
