module adsMaker

go 1.14

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/Masterminds/squirrel v1.5.0
	github.com/lib/pq v1.9.0
	github.com/stretchr/testify v1.7.0
)
