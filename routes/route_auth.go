package routes

import (
	"adsMaker/db"
	"adsMaker/utils"
	"database/sql"
	"fmt"
	"html/template"
	"net/http"
)

func Login(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	if _, err := utils.Session(_db, w, req); err == nil {
		http.Redirect(w, req, "/home", http.StatusFound)
		return
	}
	tmpl, _ := template.ParseFiles("templates/login.html")
	tmpl.Execute(w, "")
}

func Logout(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	cookie, err := req.Cookie("_cookie")
	if err != http.ErrNoCookie {
		session := db.Session{Uuid: cookie.Value}
		session.DeleteByUuid(_db)
	}
	http.Redirect(w, req, "/", http.StatusFound)
}

func signUpPost(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	if err := req.ParseForm(); err != nil {
		displayForbidden(w, req, "Cant parse form in add page.")
		return
	}
	login := req.Form["login"][0]
	initials := req.Form["initials"][0]
	pwd := req.Form["pwd"][0]
	pwdConf := req.Form["pwd_conf"][0]
	u := &db.User{Login: login,
		Initials: initials,
		Password: pwd}
	if u.AlreadyExists(_db) {
		displayForbidden(w, req, fmt.Sprintf("User %s is already exists", u.Login))
		return
	}
	if pwd != pwdConf {
		http.Redirect(w, req, "/sign_up", http.StatusFound)
		return
	}
	if err := u.AddToDb(_db); err != nil {
		displayForbidden(w, req, "Cant add user")
	}
	http.Redirect(w, req, "/login", http.StatusFound)
}

func signUpGet(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	if _, err := utils.Session(_db, w, req); err == nil {
		http.Redirect(w, req, "/home", http.StatusFound)
	} else {
		tmpl, _ := template.ParseFiles("templates/sign_up.html")
		tmpl.Execute(w, "")
	}
}

func SignUp(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		signUpPost(_db, w, req)
		return
	} else if req.Method == "GET" {
		signUpGet(_db, w, req)
		return
	}
	displayForbidden(w, req, "Unknown request method in sign up.")
}

func Auth(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	if err := req.ParseForm(); err != nil {
		http.Redirect(w, req, "/", http.StatusFound)
		return
	}

	u, err := db.GetByLogin(_db, req.FormValue("login"))
	if err != nil {
		displayForbidden(w, req, "Unknown user")
		return
	}
	if u.Password != req.FormValue("pwd") {
		displayForbidden(w, req, "Incorrect password")
		return
	}
	session, err := u.CreateSession(_db)
	if err != nil {
		displayForbidden(w, req, "Cant create session")
		return
	}
	cookie := http.Cookie{
		Name:     "_cookie",
		Value:    session.Uuid,
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
	http.Redirect(w, req, "/", http.StatusFound)
}
