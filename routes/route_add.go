package routes

import (
	"adsMaker/db"
	"adsMaker/utils"
	"database/sql"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"strings"
)

func addGet(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	if _, err := utils.Session(_db, w, req); err != nil {
		http.Redirect(w, req, "/login", http.StatusFound)
	} else {
		tmpl, _ := template.ParseFiles("templates/add_new.html")
		tmpl.Execute(w, "")
	}
}

func addPost(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	if sess, err := utils.Session(_db, w, req); err != nil {
		http.Redirect(w, req, "/login", http.StatusFound)
	} else {
		user, err := sess.GetUser(_db)
		if err != nil {
			displayForbidden(w, req, "Unknown user.")
			return
		}
		if err := req.ParseForm(); err != nil {
			displayForbidden(w, req, "Cant parse form in add page.")
			return
		}

		strPrice := req.Form["price"][0]
		price, err := strconv.Atoi(strPrice)
		photoUrls := strings.Split(req.Form["photo_urls"][0], ",")
		ad := db.Ad{UserLogin: user.Login,
			Title:       req.Form["title"][0],
			Description: req.Form["description"][0],
			PhotoUrls:   photoUrls,
			Price:       price}

		code := ad.GetStatusCode(_db)
		if code != 0 {
			displayForbidden(w, req, fmt.Sprintf("Cant add this ad to db, error code is %d", code))
			return
		}
		if _, err := ad.AddToDb(_db); err != nil {
			displayForbidden(w, req, "Cant add this ad to db")
			return
		}
		http.Redirect(w, req, "/", http.StatusFound)
	}
}

func Add(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		addPost(_db, w, req)
		return
	} else if req.Method == "GET" {
		addGet(_db, w, req)
		return
	}
	displayForbidden(w, req, "Unknown request method in add new ad.")
}
