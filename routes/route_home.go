package routes

import (
	"adsMaker/utils"
	"database/sql"
	"html/template"
	"net/http"
)

func Home(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	if sess, err := utils.Session(_db, w, req); err != nil {
		http.Redirect(w, req, "/login", http.StatusFound)
	} else {
		user, err := sess.GetUser(_db)
		if err != nil {
			displayForbidden(w, req, "Unknown user.")
			return
		}
		tmpl, _ := template.ParseFiles("templates/home.html")
		tmpl.Execute(w, user)
	}
}
