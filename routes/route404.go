package routes

import (
	"html/template"
	"net/http"
)

func ForbiddenPage(w http.ResponseWriter, req *http.Request) {
	displayForbidden(w, req, "This is page for errors")
}

func displayForbidden(w http.ResponseWriter, req *http.Request, err interface{}) {
	tmpl, _ := template.ParseFiles("templates/forbidden_page.html")
	tmpl.Execute(w, err)
}
