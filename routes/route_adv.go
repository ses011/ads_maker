package routes

import (
	"adsMaker/db"
	"database/sql"
	"html/template"
	"net/http"
	"strconv"
)

func parseAid(req *http.Request) int {
	aid, _ := strconv.Atoi(req.URL.Query().Get("aid"))
	return aid
}

func Advertisement(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	aid := parseAid(req)
	rAd, err := db.GetByAid(_db, aid)
	if err == nil {
		tmpl, _ := template.ParseFiles("templates/advertising.html")
		tmpl.Execute(w, rAd)
		return
	}
	displayForbidden(w, req, "Invalid ad id")
}
