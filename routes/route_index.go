package routes

import (
	"adsMaker/db"
	"database/sql"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

type ViewDataIndex struct {
	Items []db.Ad
	Pages []int
}

func parsePageNum(pageNumS string) (int, error) {
	var pageNum int
	var err error
	if pageNumS != "" {
		pageNum, err = strconv.Atoi(pageNumS)
		if err != nil {
			return 0, err
		}
	} else {
		pageNum = 0
	}
	return pageNum, nil
}

func Index(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	var err error
	pageNumS := req.URL.Query().Get("page")
	pageNum, err := parsePageNum(pageNumS)

	if err != nil {
		displayForbidden(w, req, "Invalid page number")
		return
	}

	var ads []db.Ad
	numOfAds := db.GetNumOfAds(_db)
	if pageNum == 0 {
		ads, err = db.GetList(_db, "price", "desc", 0)
	} else if (pageNum > 0) && (pageNum-1 <= numOfAds/db.MaxPhotoPage) {
		ads, err = db.GetList(_db, "price", "desc", uint64(int(10*(pageNum-1))))
	} else {
		displayForbidden(w, req, "Incorrect page num")
		return
	}

	if err != nil {
		fmt.Fprint(w, "something went wrong")
		return
	}

	tmpl, _ := template.ParseFiles("templates/index.html")
	var p []int
	for i := 0; i <= numOfAds/db.MaxPhotoPage; i++ {
		p = append(p, i+1)
	}
	d := ViewDataIndex{ads, p}
	tmpl.Execute(w, d)
}
