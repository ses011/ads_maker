package main

import (
	"adsMaker/api"
	"adsMaker/db"
	"adsMaker/routes"
	"adsMaker/utils"
	"log"
	"net/http"
)

func main() {
	mux := http.NewServeMux()

	Advertisement := utils.WrapCall(db.Db, routes.Advertisement)
	mux.HandleFunc("/adv", Advertisement)
	Add := utils.WrapCall(db.Db, routes.Add)
	mux.HandleFunc("/add", Add)
	mux.HandleFunc("/404", routes.ForbiddenPage)
	Index := utils.WrapCall(db.Db, routes.Index)
	mux.HandleFunc("/", Index)
	Home := utils.WrapCall(db.Db, routes.Home)
	mux.HandleFunc("/home", Home)
	Login := utils.WrapCall(db.Db, routes.Login)
	mux.HandleFunc("/login", Login)
	Logout := utils.WrapCall(db.Db, routes.Logout)
	mux.HandleFunc("/logout", Logout)
	Auth := utils.WrapCall(db.Db, routes.Auth)
	mux.HandleFunc("/auth", Auth)
	SignUp := utils.WrapCall(db.Db, routes.SignUp)
	mux.HandleFunc("/sign_up", SignUp)

	apiGetByAid := utils.WrapCall(db.Db, api.GetByAid)
	mux.HandleFunc("/api/get_by_aid", apiGetByAid)
	apiGetList := utils.WrapCall(db.Db, api.GetList)
	mux.HandleFunc("/api/get_list", apiGetList)
	apiAdd := utils.WrapCall(db.Db, api.Add)
	mux.HandleFunc("/api/add", apiAdd)
	apiAddUser := utils.WrapCall(db.Db, api.AddUser)
	mux.HandleFunc("/api/add_user", apiAddUser)

	// http://localhost:8080
	server := &http.Server{
		Addr:    "0.0.0.0:8080",
		Handler: mux,
	}

	log.Fatal(server.ListenAndServe())
}
