package api

import (
	"adsMaker/db"
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
)

type addResponse struct {
	Code int `json:"status_code"`
	Aid  int `json:"aid"`
}

func parseApiAdd(req *http.Request) db.Ad {
	res := db.Ad{}
	res.Aid, _ = strconv.Atoi(req.URL.Query().Get("id"))
	res.Title = req.URL.Query().Get("title")
	res.Price, _ = strconv.Atoi(req.URL.Query().Get("price"))
	res.UserLogin = req.URL.Query().Get("user_login")
	res.Description = req.URL.Query().Get("description")
	photoUrlsS := req.URL.Query().Get("photo_urls")
	photoUrls := strings.Split(photoUrlsS, ",")
	for _, p := range photoUrls {
		if p != "" {
			res.PhotoUrls = append(res.PhotoUrls, p)
		}
	}
	return res
}

func Add(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	rAd := parseApiAdd(req)
	code := rAd.GetStatusCode(_db)
	if code == 0 {
		aid, err := rAd.AddToDb(_db)
		if err == nil {
			w.WriteHeader(http.StatusOK)
			r := addResponse{OK, aid}
			json.NewEncoder(w).Encode(r)
			return
		}
	}
	w.WriteHeader(http.StatusBadRequest)
	r := addResponse{code, 0}
	json.NewEncoder(w).Encode(r)
}
