package api

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestValidToGetList(t *testing.T) {
	type Input struct {
		sort   string
		order  string
		offset int
	}

	var tests = []struct {
		input Input
		want  int
	}{
		{Input{"", "asc", 0}, NoSort},
		{Input{"title", "asc", 0}, IncorrectSort},
		{Input{"price", "asc", -9}, IncorrectOffset},
		{Input{"created_at", "min", 0}, IncorrectOrder},
		{Input{"price", "asc", 0}, OK},
		{Input{"price", "desc", 0}, OK},
		{Input{"created_at", "desc", 0}, OK},
		{Input{"created_at", "asc", 0}, OK},
		{Input{"created_at", "asc", 93}, OK},
	}

	for _, test := range tests {
		assert.Equal(t, test.want, validToGetList(test.input.sort, test.input.order, test.input.offset))
	}
}
