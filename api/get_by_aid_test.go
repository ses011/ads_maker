package api

import (
	"adsMaker/db"
	"adsMaker/utils"
	"encoding/json"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestApiValidToGetByAid(t *testing.T) {
	type Want struct {
		correct   bool
		desc      bool
		allPhotos bool
	}

	var tests = []struct {
		input []string
		want  Want
	}{
		{[]string{"1", "2", "3"}, Want{false, false, false}},
		{[]string{"1"}, Want{false, false, false}},
		{[]string{"description", "description"}, Want{false, true, false}},
		{[]string{"all_photos", "all_photos"}, Want{false, false, true}},
		{[]string{""}, Want{true, false, false}},
		{[]string{"all_photos"}, Want{true, false, true}},
		{[]string{"description"}, Want{true, true, false}},
		{[]string{"description", "all_photos"}, Want{true, true, true}},
		{[]string{"all_photos", "description"}, Want{true, true, true}},
	}

	for _, test := range tests {
		f1, f2, f3 := validFields(test.input)
		assert.Equal(t, test.want.correct, f1)
		assert.Equal(t, test.want.desc, f2)
		assert.Equal(t, test.want.allPhotos, f3)

	}
}

func TestParseApiGetByAid(t *testing.T) {
	t.Run("parse correct", func(t *testing.T) {
		url := "/api/get_by_aid?aid=19&fields=description,all_photos"
		req, _ := http.NewRequest("GET", url, nil)

		aid, fields := parseApiGetByAid(req)

		assert.Equal(t, 19, aid)
		assert.Equal(t, 2, len(fields))
		assert.Equal(t, "description", fields[0])
		assert.Equal(t, "all_photos", fields[1])
	})

	t.Run("parse, aid is not a num", func(t *testing.T) {
		url := "/api/get_by_aid?aid=zxc"
		req, _ := http.NewRequest("GET", url, nil)

		aid, fields := parseApiGetByAid(req)

		assert.Equal(t, 0, aid)
		assert.Equal(t, 1, len(fields))
	})
}

func TestApiGetByAid(t *testing.T) {
	columns := []string{"title", "price"}

	t.Run("get by aid, without anything", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow("title", 123)).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1")).RowsWillBeClosed()

		getByAid := utils.WrapCall(_db, GetByAid)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/get_by_aid", getByAid)
		writer := httptest.NewRecorder()

		url := "/api/get_by_aid?aid=1"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := GetByAidResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, OK, resp.Code)
		assert.Equal(t, 123, resp.A.Price)
		assert.Equal(t, "title", resp.A.Title)
		assert.Equal(t, 1, len(resp.A.PhotoUrls))
		assert.Equal(t, "p1", resp.A.PhotoUrls[0])
		assert.Equal(t, "", resp.A.Description)
	})

	t.Run("get by aid, with description", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(append(columns, "description")).AddRow("title", 123, "desc")).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1")).
			RowsWillBeClosed()

		getByAid := utils.WrapCall(_db, GetByAid)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/get_by_aid", getByAid)
		writer := httptest.NewRecorder()

		url := "/api/get_by_aid?aid=1&fields=description"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := GetByAidResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, OK, resp.Code)
		assert.Equal(t, 123, resp.A.Price)
		assert.Equal(t, "title", resp.A.Title)
		assert.Equal(t, 1, len(resp.A.PhotoUrls))
		assert.Equal(t, "p1", resp.A.PhotoUrls[0])
		assert.Equal(t, "desc", resp.A.Description)
	})

	t.Run("get by aid, with all_photos", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(columns).AddRow("title", 123)).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1").AddRow("p2")).
			RowsWillBeClosed()

		getByAid := utils.WrapCall(_db, GetByAid)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/get_by_aid", getByAid)
		writer := httptest.NewRecorder()

		url := "/api/get_by_aid?aid=1&fields=all_photos"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := GetByAidResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, OK, resp.Code)
		assert.Equal(t, 123, resp.A.Price)
		assert.Equal(t, "title", resp.A.Title)
		assert.Equal(t, 2, len(resp.A.PhotoUrls))
		assert.Equal(t, "p1", resp.A.PhotoUrls[0])
		assert.Equal(t, "p2", resp.A.PhotoUrls[1])
		assert.Equal(t, "", resp.A.Description)
	})

	t.Run("get by aid, both fields", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(append(columns, "description")).AddRow("title", 123, "desc")).
			RowsWillBeClosed()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"photo_url"}).AddRow("p1").AddRow("p2")).
			RowsWillBeClosed()

		getByAid := utils.WrapCall(_db, GetByAid)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/get_by_aid", getByAid)
		writer := httptest.NewRecorder()

		url := "/api/get_by_aid?aid=1&fields=all_photos,description"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := GetByAidResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, OK, resp.Code)
		assert.Equal(t, 123, resp.A.Price)
		assert.Equal(t, "title", resp.A.Title)
		assert.Equal(t, 2, len(resp.A.PhotoUrls))
		assert.Equal(t, "p1", resp.A.PhotoUrls[0])
		assert.Equal(t, "p2", resp.A.PhotoUrls[1])
		assert.Equal(t, "desc", resp.A.Description)
	})

	t.Run("get by aid, ad not exists", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`)

		getByAid := utils.WrapCall(_db, GetByAid)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/get_by_aid", getByAid)
		writer := httptest.NewRecorder()

		url := "/api/get_by_aid?aid=1&fields=all_photos,description"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := GetByAidResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, IncorrectAid, resp.Code)
		assert.Equal(t, db.Ad{}, resp.A)
	})

	t.Run("get by aid, incorrect fields", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		getByAid := utils.WrapCall(_db, GetByAid)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/get_by_aid", getByAid)
		writer := httptest.NewRecorder()

		url := "/api/get_by_aid?aid=1&fields=price,description"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := GetByAidResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, IncorrectFields, resp.Code)
		assert.Equal(t, db.Ad{}, resp.A)
	})
}
