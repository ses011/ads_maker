package api

import (
	"adsMaker/db"
	"database/sql"
	"encoding/json"
	"net/http"
)

type AddUserResponse struct {
	Code  int    `json:"status_code"`
	Login string `json:"login"`
}

func parseApiAddUser(req *http.Request) db.User {
	res := db.User{}
	res.Login = req.URL.Query().Get("login")
	res.Password = req.URL.Query().Get("password")
	res.Initials = req.URL.Query().Get("initials")
	return res
}

func AddUser(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	rU := parseApiAddUser(req)
	if !rU.AlreadyExists(_db) {
		code := rU.GetStatusCode()
		if code == 0 {
			if err := rU.AddToDb(_db); err == nil {
				w.WriteHeader(http.StatusOK)
				r := AddUserResponse{OK, rU.Login}
				json.NewEncoder(w).Encode(r)
				return
			}
		}
		w.WriteHeader(http.StatusBadRequest)
		r := AddUserResponse{code, ""}
		json.NewEncoder(w).Encode(r)
		return
	}
	w.WriteHeader(http.StatusBadRequest)
	r := AddUserResponse{UserExists, ""}
	json.NewEncoder(w).Encode(r)
}
