package api

import (
	"adsMaker/db"
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
)

type GetByAidResponse struct {
	Code int   `json:"status_code"`
	A    db.Ad `json:"ad"`
}

func parseApiGetByAid(req *http.Request) (int, []string) {
	aid, _ := strconv.Atoi(req.URL.Query().Get("aid"))
	fields := strings.Split(req.URL.Query().Get("fields"), ",")
	return aid, fields
}

func validFields(fields []string) (bool, bool, bool) {
	if len(fields) > 2 {
		return false, false, false
	}
	d, a := false, false
	for _, f := range fields {
		if f == "" {
			continue
		}
		if f != "description" && f != "all_photos" {
			return false, d, a
		}
		if f == "description" && !d {
			d = true
		} else if f == "description" && d {
			return false, d, a
		} else if f == "all_photos" && !a {
			a = true
			continue
		} else {
			return false, d, a
		}

	}
	return true, d, a
}

func GetByAid(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	aid, fields := parseApiGetByAid(req)
	if t, d, a := validFields(fields); t {
		rAd, err := db.GetByAidWithParams(_db, aid, d, a)
		if err == nil {
			w.WriteHeader(http.StatusOK)
			r := GetByAidResponse{OK, rAd}
			json.NewEncoder(w).Encode(r)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		r := GetByAidResponse{IncorrectAid, db.Ad{}}
		json.NewEncoder(w).Encode(r)
		return
	}
	w.WriteHeader(http.StatusBadRequest)
	r := GetByAidResponse{IncorrectFields, db.Ad{}}
	json.NewEncoder(w).Encode(r)
}
