package api

const (
	OK int = 0
)

const (
	NoSort int = iota + 21
	IncorrectSort
	IncorrectOrder
	IncorrectOffset
)

const (
	IncorrectAid int = iota + 31
	IncorrectFields
)

const (
	UserExists int = 47
)
