package api

import (
	"adsMaker/db"
	"adsMaker/utils"
	"encoding/json"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestApiAdd(t *testing.T) {
	userColumns := []string{"uid", "login", "password", "initials"}

	t.Run("add with description", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		q := `INSERT INTO ads (.+) returning aid`

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(userColumns).AddRow(1, "foo", "12345678", "fff")).
			RowsWillBeClosed()
		mock.ExpectQuery(q).
			WithArgs("foo", "title", "description", 123).
			WillReturnRows(sqlmock.NewRows([]string{"aid"}).AddRow(1)).
			RowsWillBeClosed()
		mock.ExpectExec("INSERT INTO photo_urls").
			WithArgs("p1", 1).
			WillReturnResult(sqlmock.NewResult(1, 1))

		Add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", Add)
		writer := httptest.NewRecorder()

		url := "/api/add?user_login=foo&title=title&description=description&price=123&photo_urls=p1"

		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, 0, resp.Code)
		assert.Equal(t, 1, resp.Aid)
	})

	t.Run("add without description", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		q := `INSERT INTO ads (.+) returning aid`

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(userColumns).AddRow(1, "foo", "12345678", "fff")).
			RowsWillBeClosed()
		mock.ExpectQuery(q).
			WithArgs("foo", "title", "", 123).
			WillReturnRows(sqlmock.NewRows([]string{"aid"}).AddRow(1)).
			RowsWillBeClosed()
		mock.ExpectExec("INSERT INTO photo_urls").
			WithArgs("p1", 1).
			WillReturnResult(sqlmock.NewResult(1, 1))

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		url := "/api/add?user_login=foo&title=title&price=123&photo_urls=p1"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, 0, resp.Code)
		assert.Equal(t, 1, resp.Aid)
	})

	t.Run("add, no user", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		url := "/api/add?title=title&price=123&photo_urls=p1"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.NoUserLogin, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})

	t.Run("add, user not exists", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`)

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		url := "/api/add?user_login=zxc&title=title&price=123&photo_urls=p1"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.IncorrectUserLogin, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})

	t.Run("add, user not exists", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`)

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		url := "/api/add?user_login=zxc&title=title&price=123&photo_urls=p1"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.IncorrectUserLogin, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})

	t.Run("add, no title", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(userColumns).AddRow(1, "foo", "12345678", "fff")).
			RowsWillBeClosed()

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		url := "/api/add?user_login=foo&price=123&photo_urls=p1"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.NoTitle, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})

	t.Run("add, large title", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(userColumns).AddRow(1, "foo", "12345678", "fff")).
			RowsWillBeClosed()

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		title := strings.Repeat("a", 201)
		url := fmt.Sprintf("/api/add?user_login=foo&title=%s&price=123&photo_urls=p1", title)
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.IncorrectTitle, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})

	t.Run("add, large title", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(userColumns).AddRow(1, "foo", "12345678", "fff")).
			RowsWillBeClosed()

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		title := strings.Repeat("a", 201)
		url := fmt.Sprintf("/api/add?user_login=foo&title=%s&price=123&photo_urls=p1", title)
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.IncorrectTitle, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})

	t.Run("add,  no photos", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(userColumns).AddRow(1, "foo", "12345678", "fff")).
			RowsWillBeClosed()

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		url := "/api/add?user_login=foo&title=title&price=123&"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.NoPhotos, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})

	t.Run("add,  4 photos", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(userColumns).AddRow(1, "foo", "12345678", "fff")).
			RowsWillBeClosed()

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		url := "/api/add?user_login=foo&title=title&price=123&photo_urls=p1,p2,p3,p4"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.IncorrectPhotosAmount, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})

	t.Run("add,  no price", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(userColumns).AddRow(1, "foo", "12345678", "fff")).
			RowsWillBeClosed()

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		url := "/api/add?user_login=foo&title=title&photo_urls=p1,p2,p3"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.NoPrice, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})

	t.Run("add,  negative price", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(userColumns).AddRow(1, "foo", "12345678", "fff")).
			RowsWillBeClosed()

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		url := "/api/add?user_login=foo&title=title&photo_urls=p1,p2,p3&price=-9"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.NegativePrice, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})

	t.Run("add,  large description", func(t *testing.T) {
		_db, mock, _ := sqlmock.New()
		defer _db.Close()
		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows(userColumns).AddRow(1, "foo", "12345678", "fff")).
			RowsWillBeClosed()

		add := utils.WrapCall(_db, Add)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add", add)
		writer := httptest.NewRecorder()

		description := strings.Repeat("a", 1002)
		url := fmt.Sprintf("/api/add?user_login=foo&title=title&photo_urls=p1,p2,p3&price=123&description=%s",
			description)
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := addResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.IncorrectDescription, resp.Code)
		assert.Equal(t, 0, resp.Aid)
	})
}

func TestParseApiAdd(t *testing.T) {
	t.Run("parse correct request", func(t *testing.T) {
		url := "/api/add?user_login=foo&title=title&photo_urls=p1,p2&price=9&description=description"
		req, _ := http.NewRequest("GET", url, nil)

		a := parseApiAdd(req)

		assert.Equal(t, "foo", a.UserLogin)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, 9, a.Price)
		assert.Equal(t, "description", a.Description)
		assert.Equal(t, 2, len(a.PhotoUrls))
		assert.Equal(t, "p1", a.PhotoUrls[0])
		assert.Equal(t, "p2", a.PhotoUrls[1])
	})

	t.Run("parse price is not a num", func(t *testing.T) {
		url := "/api/add?user_login=foo&title=title&photo_urls=p1,p2,p3&price=abc&description=description"
		req, _ := http.NewRequest("GET", url, nil)

		a := parseApiAdd(req)

		assert.Equal(t, "foo", a.UserLogin)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, 0, a.Price)
		assert.Equal(t, "description", a.Description)
		assert.Equal(t, 3, len(a.PhotoUrls))
	})

	t.Run("parse without photos", func(t *testing.T) {
		url := "/api/add?user_login=foo&title=title&price=abc&description=description&photo_urls=,,,,"
		req, _ := http.NewRequest("GET", url, nil)

		a := parseApiAdd(req)

		assert.Equal(t, "foo", a.UserLogin)
		assert.Equal(t, "title", a.Title)
		assert.Equal(t, 0, a.Price)
		assert.Equal(t, "description", a.Description)
		assert.Equal(t, 0, len(a.PhotoUrls))
	})
}
