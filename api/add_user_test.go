package api

import (
	"adsMaker/db"
	"adsMaker/utils"
	"encoding/json"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestParseApiAddUser(t *testing.T) {
	t.Run("parse correct", func(t *testing.T) {
		url := "/api/add_user?login=99999&password=12345678&initials=initials test"
		req, _ := http.NewRequest("GET", url, nil)

		u := parseApiAddUser(req)

		assert.Equal(t, "99999", u.Login)
		assert.Equal(t, "12345678", u.Password)
		assert.Equal(t, "initials test", u.Initials)
	})
}

func TestApiAddUser(t *testing.T) {
	t.Run("add user correct", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0)).
			RowsWillBeClosed()
		mock.ExpectExec("INSERT INTO users").
			WithArgs("foo", "12345678", "test ads maker").
			WillReturnResult(sqlmock.NewResult(1, 1))

		addUser := utils.WrapCall(_db, AddUser)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add_user", addUser)
		writer := httptest.NewRecorder()

		url := "/api/add_user?login=foo&password=12345678&initials=test ads maker"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusOK, writer.Code)

		resp := AddUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, OK, resp.Code)
		assert.Equal(t, "foo", resp.Login)
	})

	t.Run("add user without login", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0)).
			RowsWillBeClosed()

		addUser := utils.WrapCall(_db, AddUser)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add_user", addUser)
		writer := httptest.NewRecorder()

		url := "/api/add_user?password=12345678&initials=test ads maker"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := AddUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.NoLogin, resp.Code)
		assert.Equal(t, "", resp.Login)
	})

	t.Run("add user with large login", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0)).
			RowsWillBeClosed()

		addUser := utils.WrapCall(_db, AddUser)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add_user", addUser)
		writer := httptest.NewRecorder()

		login := strings.Repeat("a", 21)
		url := fmt.Sprintf("/api/add_user?login=%vpassword=12345678&initials=test ads maker", login)
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := AddUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.IncorrectLogin, resp.Code)
		assert.Equal(t, "", resp.Login)
	})

	t.Run("add user without password", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0)).
			RowsWillBeClosed()

		addUser := utils.WrapCall(_db, AddUser)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add_user", addUser)
		writer := httptest.NewRecorder()

		url := "/api/add_user?login=foo&initials=test ads maker"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := AddUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.NoPassword, resp.Code)
		assert.Equal(t, "", resp.Login)
	})

	t.Run("add user with large password", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0)).
			RowsWillBeClosed()

		addUser := utils.WrapCall(_db, AddUser)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add_user", addUser)
		writer := httptest.NewRecorder()

		password := strings.Repeat("a", 41)
		url := fmt.Sprintf("/api/add_user?login=foo&password=%v&initials=test ads maker", password)
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := AddUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.IncorrectPassword, resp.Code)
		assert.Equal(t, "", resp.Login)
	})

	t.Run("add user with small password", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0)).
			RowsWillBeClosed()

		addUser := utils.WrapCall(_db, AddUser)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add_user", addUser)
		writer := httptest.NewRecorder()

		password := strings.Repeat("a", 4)
		url := fmt.Sprintf("/api/add_user?login=foo&password=%v&initials=test ads maker", password)
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := AddUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.IncorrectPassword, resp.Code)
		assert.Equal(t, "", resp.Login)
	})

	t.Run("add user without initials", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0)).
			RowsWillBeClosed()

		addUser := utils.WrapCall(_db, AddUser)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add_user", addUser)
		writer := httptest.NewRecorder()

		url := "/api/add_user?login=foo&password=12345678"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := AddUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.NoInitials, resp.Code)
		assert.Equal(t, "", resp.Login)
	})

	t.Run("add user with large initials", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(0)).
			RowsWillBeClosed()

		addUser := utils.WrapCall(_db, AddUser)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add_user", addUser)
		writer := httptest.NewRecorder()

		initials := strings.Repeat("a", 61)
		url := fmt.Sprintf("/api/add_user?login=foo&password=12345678&initials=%v", initials)
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := AddUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, db.IncorrectInitials, resp.Code)
		assert.Equal(t, "", resp.Login)
	})

	t.Run("add user already exists", func(t *testing.T) {

		_db, mock, _ := sqlmock.New()
		defer _db.Close()

		mock.ExpectQuery(`SELECT`).
			WillReturnRows(sqlmock.NewRows([]string{"count"}).AddRow(1)).
			RowsWillBeClosed()

		addUser := utils.WrapCall(_db, AddUser)
		mux := http.NewServeMux()
		mux.HandleFunc("/api/add_user", addUser)
		writer := httptest.NewRecorder()

		url := "/api/add_user?login=foo&password=12345678&initials=test ads maker"
		req, _ := http.NewRequest("GET", url, nil)
		mux.ServeHTTP(writer, req)

		assert.NoError(t, mock.ExpectationsWereMet())
		assert.Equal(t, http.StatusBadRequest, writer.Code)

		resp := AddUserResponse{}
		json.NewDecoder(writer.Body).Decode(&resp)

		assert.Equal(t, UserExists, resp.Code)
		assert.Equal(t, "", resp.Login)
	})
}
