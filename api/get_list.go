package api

import (
	"adsMaker/db"
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
)

type getListResponse struct {
	Code   int     `json:"status_code"`
	AdList []db.Ad `json:"info"`
}

func parseApiGetList(req *http.Request) (string, string, int) {
	sort := req.URL.Query().Get("sort")
	order := req.URL.Query().Get("order")
	if order == "" {
		order = "desc"
	}
	offset, _ := strconv.Atoi(req.URL.Query().Get("offset"))
	return sort, order, offset
}

func validToGetList(sort string, order string, offset int) int {
	if sort == "" {
		return NoSort
	}
	if strings.ToLower(sort) != "price" && strings.ToLower(sort) != "created_at" {
		return IncorrectSort
	}
	if offset < 0 {
		return IncorrectOffset
	}
	if strings.ToLower(order) != "asc" && strings.ToLower(order) != "desc" {
		return IncorrectOrder
	}
	return OK
}

func GetList(_db *sql.DB, w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	sort, order, offset := parseApiGetList(req)
	v := validToGetList(sort, order, offset)
	if v == OK {
		adS, err := db.GetList(_db, sort, order, uint64(offset))
		if err == nil {
			w.WriteHeader(http.StatusOK)
			r := getListResponse{OK, adS}
			json.NewEncoder(w).Encode(r)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(nil)
	}
	w.WriteHeader(http.StatusBadRequest)
	r := getListResponse{v, nil}
	json.NewEncoder(w).Encode(r)
}
